# Rockbasher stats

Une petite application hybride pour visualiser les données récolté du jeu **Rockbasher** que l'on peut jouer [ici](https://www.rockbasher.com/).

## Technologies

- PhoneGap cli-6.1.0
- Framework7 2.0.7
- Vue 2.5.2
- Vue-Chartjs 3.3.1
- node.js 8.11.2 LTS
- yarn 1.6.0
- axios 0.18.0

## Installation

1. Télécharger et installer **node.js LTS**
2. Télécharger et installer **yarn**
3. Ouvrez une console à l'emplacement où vous voulez installer le projet et lancez la commande `git clone git@gitlab.com:LogLauncher/diplome-rockbasher_stats.git`
4. Quand le clone est terminé lancez la commande `cd diplome-rockbasher_stats`
5. Lancez la commande `yarn install`
6. Une fois tous les packages installé il suffit de lancez la commande `yarn run dev`
7. Ouvrez votre navigateur favori puis entrez l'url suivante [http://localhost:8080/](http://localhost:8080/)

## Build l'application PhoneGap

1. Ouvrez une console à l'emplacement de votre projet et executer `yarn run build`
2. Une fois terminé faite une archive du contenue du dossier **www/**
3. Connectez-vous sur **Adobe® PhoneGap™ Build** et uploader le fichier zip
4. Une fois uploader votre application sera build pour les divers appareil.
