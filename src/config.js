export default {
  apiUrl: 'http://ec2-52-15-128-94.us-east-2.compute.amazonaws.com/api/',
  defaultChartOptions: {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom'
    }
  },
  colors: [
    '255, 153, 0',
    '0, 110, 144',
    '97, 3, 69',
    '237, 28, 36',
    '91, 75, 73'
  ]
};
