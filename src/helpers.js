import moment from 'moment';

export default {
  pad(num) {
    return ('0' + num).slice(-2);
  },
  hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs = secs % 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return hours + ':' + this.pad(minutes) + ':' + this.pad(secs);
  },
  formatDate(date = moment.now(), format = 'YYYY-MM-DD') {
    return moment(date).format(format);
  },
  capitalizeFirstLetter(value) {
    return value.charAt(0).toUpperCase() + value.substr(1);
  },
  f7Alert(title, message) {
    const APP = this.$f7;
    APP.dialog.alert(
      message,
      title
    );
  },
  axiosErrorHandler(error) {
    const APP = this.$f7;
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      APP.dialog.alert(
        null,
        `Error ${error.response.status} - ${error.response.statusText}`,
      );
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      if (error.request.status === 0) {
        APP.dialog.alert(
          null,
          'Error connecting to server'
        );
      }
    } else {
      // Something happened in setting up the request that triggered an Error
      APP.dialog.alert(
        error.message,
        'Error in request setup'
      );
    }
  }
};
