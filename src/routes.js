import Home from './pages/Home';
import DayPlayers from './pages/DayPlayers';
import FrequencyPlayers from './pages/FrequencyPlayers';
import AveragePlayMap from './pages/AveragePlayMap';
import PlayersTime from './pages/PlayersTime';
import NotFoundPage from './pages/not-found';
import PlayTimeWeek from './pages/PlayTimeWeek';
import PlayersAbsent from './pages/PlayersAbsent';
import PlayerList from './components/PlayerList';
import NewPlayersWeek from './pages/NewPlayersWeek';
import WinsFailsMap from './pages/WinsFailsMap';
import FailsMapPlayer from './pages/FailsMapPlayer';

export default [
  {
    path: '/',
    component: Home
  },
  {
    path: '/dayplayers/',
    component: DayPlayers
  },
  {
    path: '/frequencyplayers/',
    component: FrequencyPlayers
  },
  {
    path: '/averageplaymap/',
    component: AveragePlayMap
  },
  {
    path: '/playerstime/',
    component: PlayersTime
  },
  {
    path: '/playtimeweek/',
    component: PlayTimeWeek
  },
  {
    path: '/playersabsent/',
    component: PlayersAbsent
  },
  {
    path: '/playerlist/',
    component: PlayerList
  },
  {
    path: '/newplayersweek/',
    component: NewPlayersWeek
  },
  {
    path: '/winsfailsmap/',
    component: WinsFailsMap
  },
  {
    path: '/failsmapplayer/',
    component: FailsMapPlayer
  },
  {
    path: '(.*)',
    component: NotFoundPage
  }
];
